<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormGroupServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    /* 
    Example Usage: at .blade.php with "use Form;"
    {{ Form::textGroup([
        'name' => 'firstname',
        'value' => $user->firstname,
        'label' => 'First Name',
    ], $errors) }}
    */
    public function boot()
    {
        //
        Form::component('textGroup','components.text', ['params','errors']);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

