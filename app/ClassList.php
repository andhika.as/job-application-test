<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class ClassList extends Model
{
    //use Notifiable;
    protected $table = 'class';
    // protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'time', 'room', 'teacher_id', 'isOpen', 'semester'
    ];
    // teacher_id also auto fill by teacher user, editable by admin
    // studentCount auto Iterate

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    // ];

    public function teacher_id(){
        return $this->hasOne('App\Teacher');
    }
    
}