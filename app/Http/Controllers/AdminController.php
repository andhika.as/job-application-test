<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassList;
use App\Student;
use App\Teacher;
use View;
use PDF;
//use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function addClass(){
        $lists = ClassList::all();
        return View::make('adminApp/addClass', compact('lists'));
    }

    public function addTeacher(){
        $lists = Teacher::all();
        return View::make('adminApp/addTeacher', compact('lists'));
    }
    
    public function addStudent(){
        $lists = Student::all();
        return View::make('adminApp/addStudent', compact('lists'));
    }

    public function downloadPDF($page){
        switch($page){
            case 'class':
                $lists = ClassList::all();
                break;
            case 'teacher':
                $lists = Teacher::all();
                break;
            case 'student':
                $lists = Student::all();
                break;
            // default:
            //     return redirect()->back()->with('failed', "Something is fishy");
        }
        //dd($page);
        $page = "pdf/{$page}";
        $pdf = PDF::loadView($page, compact('lists'));
        return $pdf->download('invoice.pdf');
    }

    public function editClass($id){
        $list = Classlist::findOrFail($id);
        return View::make('adminApp/editClass', compact('list', 'id'));
    }
    public function editStudent($id){
        $list = Student::findOrFail($id);
        return View::make('adminApp/editStudent', compact('list', 'id'));
    }
    public function editTeacher($id){
        $list = Teacher::findOrFail($id);
        return View::make('adminApp/editTeacher', compact('list', 'id'));
    }

}