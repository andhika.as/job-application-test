<?php

namespace App\Http\Controllers\AdminApp;

use App\User;
use App\ClassList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Validator;

class ClassController extends Controller
{

   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    protected function create(Request $data)
    {
        ClassList::create([
            'name'      => $data['name'],
            'time'      => $data['time'],
            'room'      => $data['room'],
            'teacher_id' => $data['teacher_id'],
            'isOpen'    => $data['isOpen'],
            'semester'  => $data['semester'],
        ]);
        $data->session()->flash('flash_message', 'Class successfully added!');
        return redirect()->route('class.create')->with('success', "User was successfully created..!");
    }

    protected function edit($id){
        $list = Classlist::findOrFail($id);
        return redirect()->back()->withTask($edit);
    }

    protected function modify(Request $data, $id){
        $list = Classlist::find($id);
        $list->name     = $data->name ; 
        $list->time     = $data->time ; 
        $list->room     = $data->room ; 
        $list->teacher_id= $data->teacher_id ; 
        $list->isOpen  = $data->isOpen ; 
        $list->semester  = $data->semester ;
        $data->session()->flash('flash_message', 'Class successfully Modified!');
        return redirect()->route('class.create');
    }

    protected function delete($id){
        $list = ClassList::findOrFail($id);
        $list->delete();
        //$data->session()->flash('flash_message', 'Class deleted!');
        return redirect()->route('class.create');
    }
}