<?php

namespace App\Http\Controllers\AdminApp;

use App\User;
use App\Student;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{

   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    protected function create(Request $data)
    {
        Student::create([
            'name'      => $data['name'],
            'npm'       => $data['npm'],
            'classId'   => $data['classId'],
        ]);
        $data->session()->flash('flash_message', 'Student successfully added!');
        return redirect()->route('student.create')->with('success', "User was successfully created..!");
    }

    protected function edit($id){
        $list = Student::findOrFail($id);
        return redirect()->back()->withTask($edit);
    }

    protected function modify(Request $data, $id){
        $list = Student::find($id);
        $list->name     = $data->name ; 
        $list->npm      = $data->npm ; 
        $list->classId  = $data->classId ;
        $data->session()->flash('flash_message', 'Class successfully Modified!');
        return redirect()->route('student.create');
    }

    protected function delete($id){
        $list = Student::findOrFail($id);
        $list->delete();
        //$data->session()->flash('flash_message', 'Class deleted!');
        return redirect()->route('student.create');
    }
}