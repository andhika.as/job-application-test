<?php

namespace App\Http\Controllers\AdminApp;

use App\User;
use App\Teacher;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller
{

   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    protected function create(Request $data)
    {
        Teacher::create([
            'name'      => $data['name'],
            'isActive'  => $data['isActive'],
            'reg_id'    => $data['reg_id'],
        ]);
        $data->session()->flash('flash_message', 'Teacher successfully added!');
        return redirect()->route('teacher.create')->with('success', "User was successfully created..!");
    }

    protected function edit($id){
        $list = Teacher::findOrFail($id);
        return redirect()->back()->withTask($edit);
    }

    protected function modify(Request $data, $id){
        $list = Teacher::find($id);
        $list->name     = $data->name ; 
        $list->isActive = $data->isActive; 
        $list->reg_id   = $data->reg_id ; 
        $data->session()->flash('flash_message', 'Class successfully Modified!');
        return redirect()->route('teacher.create');
    }

    protected function delete($id){
        $list = Teacher::findOrFail($id);
        $list->delete();
        //$data->session()->flash('flash_message', 'Class deleted!');
        return redirect()->route('teacher.create');
    }
}