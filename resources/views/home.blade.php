@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(\Session::has('error'))

                    <div class="alert alert-danger">
                        {{\Session::get('error')}}
                    </div>
                    @endif
                    
                    <div class=”panel-heading”>Dashboard</div>

                        <?php if(auth()->user()->role == "admin"){?>
                        <div class="panel-body">

                            <a href="{{url('admin/routes')}}">Admin</a>
                        </div>
                        <?php }
                            else{ echo '<div class="panel-heading">Role: ';
                                echo auth()->user()->role;
                                echo '</div>';
                            }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
