@extends('admin')

@section('subcontent')

    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Class - id: {{$id}}</div>

                    <div class="card-body">
                        <form method="PATCH" action="{{ route('class.modify', $id) }}" aria-label="{{ __('class.modify') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" name="name" value="{{ $list->name }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('time') }}</label>

                                <div class="col-md-6">
                                    <input id="time" type="text" name="time" value="{{ $list->time }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="room" class="col-md-4 col-form-label text-md-right">{{ __('room') }}</label>

                                <div class="col-md-6">
                                    <input id="room" type="text" name="room" value="{{ $list->room }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="teacher_id" class="col-md-4 col-form-label text-md-right">Techer ID</label>

                                <div class="col-md-6">
                                    <input id="teacher_id" type="text" name="teacher_id" value="{{ $list->teacher_id }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="isOpen" class="col-md-4 col-form-label text-md-right">Is Open?</label>

                                <div class="col-md-6">
                                <select id="isOpen" name="isOpen" value="{{ $list->isOpen}}">
                                        <option value="true">Yes</option>
                                        <option value="false">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="semester" class="col-md-4 col-form-label text-md-right">{{ __('semester') }}</label>

                                <div class="col-md-6">
                                    <input id="semester" type="text" name="semester" value="{{ $list->semester }}">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection