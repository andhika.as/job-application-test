@extends('admin')

@section('subcontent')
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Teacher</div>

                    <div class="card-body">
                    {!! Form::open(['route' => 'teacher.create']) !!}
                    @csrf
                        Name: {{!! Form::text('name') !!}} <br>
                        reg_id:{{!! Form::text('reg_id') !!}} <br>
                        isActive: {{!! Form::select('isActive', [true => 'Yes', false => 'no' ], true) !!}}<br>
                        {{!!Form::submit('Save')!!}}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-primary" href="{{ route('downloadPDF', ['page' => 'teacher'] ) }}"> 
            Download Data PDF
        </a>
        <!-- List Class -->
        <div class="card-header">
                Teacher Table
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th> id</th>
                        <th> name</th>
                        <th> reg_id  </th>
                        <th> isActive   </th>
                        <th> classCount  </th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($lists as $list)
                    <tr>
                        <td> {{$list -> id        }} </td>
                        <td> {{$list -> name      }} </td>
                        <td> {{$list -> reg_id    }} </td>
                        <td> {{$list -> isActive  }} </td>
                        <td> {{$list -> classCount}} </td>
                        <td>
                            <a class="btn btn-default" href="{{ route('teacher.edit', ['id' => $list->id] ) }}">Edit</a>
                            <form method="POST" action="{{route('teacher.delete', ['id' => $list->id ]) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger">Delete</a>
                                <input type="hidden" name="_method" value="DELETE">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection