@extends('admin')

@section('subcontent')
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Student  - id: {{$id}}</div>

                <div class="card-body">
                {!! Form::model($id, ['route' => ['student.modify', $list->id], 'method' => 'patch']) !!}
                @csrf
                    {!! Form::textGroup([
                        'name' => 'name',
                        'label' => 'Student name',
                        'value' => $list->name
                    ], $errors) !!}
                    
                    {!! Form::textGroup([
                        'name' => 'npm',
                        'label' => 'Nomor Induk mStudent',
                        'value' => $list->npm
                    ],$errors) !!}
                    
                    {!! Form::textGroup([
                        'name' => 'classId',
                        'label' => 'Registered ID class',
                        'value' => $list->classId
                    ], $errors) !!}
                    {!!Form::submit('Save')!!}
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection