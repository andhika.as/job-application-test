@extends('admin')

@section('subcontent')

    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Class</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('class.create') }}" aria-label="{{ __('class.create') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" name="name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('time') }}</label>

                                <div class="col-md-6">
                                    <input id="time" type="text" name="time" value="{{ old('time') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="room" class="col-md-4 col-form-label text-md-right">{{ __('room') }}</label>

                                <div class="col-md-6">
                                    <input id="room" type="text" name="room" value="{{ old('room') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="teacher_id" class="col-md-4 col-form-label text-md-right">Techer ID</label>

                                <div class="col-md-6">
                                    <input id="teacher_id" type="text" name="teacher_id" value="{{ old('teacher_id') }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="isOpen" class="col-md-4 col-form-label text-md-right">Is Open?</label>

                                <div class="col-md-6">
                                    <select id="isOpen" name="isOpen" value="true">
                                        <option value="true">Yes</option>
                                        <option value="false">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="semester" class="col-md-4 col-form-label text-md-right">{{ __('semester') }}</label>

                                <div class="col-md-6">
                                    <input id="semester" type="text" name="semester" value="{{ old('semester') }}">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <a class="btn btn-primary" href="{{ route('downloadPDF', ['page' => 'class'] ) }}"> 
            Download Data PDF
        </a>

        <!-- List Class -->
        <div class="card-header">
            Class Table
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th> id</th>
                        <th> name</th>
                        <th> time </th>
                        <th> room </th>
                        <th> teacher_id</th>
                        <th> isOpen </th>
                        <th> semester</th>
                        <th> Action </th> 
                    </tr>
                </thead>
                <tbody>
                    @foreach($lists as $list)
                    <tr>
                        <td> {{$list -> id        }} </td>
                        <td> {{$list -> name      }} </td>
                        <td> {{$list -> time      }} </td>
                        <td> {{$list -> room      }} </td>
                        <td> {{$list -> teacher_id}} </td>
                        <td> {{$list -> isOpen    }} </td>
                        <td> {{$list -> semester  }} </td>
                        <td>
                            <a class="btn btn-default" href="{{ route('class.edit', [$list->id] ) }}">Edit</a>
                            <form method="POST" action="{{route('class.delete', [$list->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger">Delete</a>
                                <input type="hidden" name="_method" value="DELETE">    
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--end List Class-->
    </div>
@endsection