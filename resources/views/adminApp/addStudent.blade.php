@extends('admin')

@section('subcontent')
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Student</div>

                <div class="card-body">
                {!! Form::open(['route' => 'student.create']) !!}
                @csrf
                    {!! Form::textGroup([
                        'name' => 'name',
                        'label' => 'Student name'
                    ], $errors) !!}
                    
                    {!! Form::textGroup([
                        'name' => 'npm',
                        'label' => 'Nomor Induk mStudent',
                    ],$errors) !!}
                    
                    {!! Form::textGroup([
                        'name' => 'classId',
                        'label' => 'Registered ID class'
                    ], $errors) !!}
                    {!!Form::submit('Save')!!}
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <a class="btn btn-primary" href="{{ route('downloadPDF', ['page' => 'student'] ) }}"> 
        Download Data PDF
    </a>
    <!-- List Class -->
    <div class="card-header">
            Student Table
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th> id</th>
                    <th> name</th>
                    <th> npm </th>
                    <th> classId </th>
                    <th> Action </th>
                </tr>
            </thead>
            <tbody>
                @foreach($lists as $list)
                <tr>
                    <td> {{$list -> id        }} </td>
                    <td> {{$list -> name      }} </td>
                    <td> {{$list -> npm       }} </td>
                    <td> {{$list -> classId   }} </td>
                    <td>
                        <a class="btn btn-default" href="{{ route('student.edit', [$list->id] ) }}">Edit</a>
                        <form method="POST" action="{{route('student.delete', [$list->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Delete</a>
                            <input type="hidden" name="_method" value="DELETE">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection