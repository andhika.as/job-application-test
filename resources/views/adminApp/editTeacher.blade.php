@extends('admin')

@section('subcontent')
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Teacher  - id: {{$id}}</div>

                    <div class="card-body">
                    {!! Form::model($id, ['route' => ['teacher.modify', $list->id], 'method' => 'patch']) !!}
                    @csrf
                        Name: {{!! Form::text('name', $list->name ) !!}} <br>
                        reg_id:{{!! Form::text('reg_id', $list->reg_id) !!}} <br>
                        isActive: {{!! Form::select('isActive', [true => 'Yes', false => 'no' ], $list->isActive ) !!}}<br>
                        {{!!Form::submit('Save')!!}}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection