@extends('layouts.app')

@section('content')

    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a class="btn btn" href="/admin/addClass">Class Table</a>
        <a class="btn btn" href="/admin/addTeacher">Teacher Table</a>
        <a class="btn btn" href="/admin/addStudent">Student Table</a>
    </div>

    <!-- Use any element to open the sidenav -->
    <span onclick="openNav()">open</span>

    <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
    <div id="main">
        @yield('subcontent')
    </div> 

@endsection