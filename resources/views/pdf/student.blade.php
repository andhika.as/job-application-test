<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    
        <div class="card-header">
                Student Table
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th> id</th>
                        <th> name</th>
                        <th> npm </th>
                        <th> classId </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($lists as $list)
                    <tr>
                        <td> {{$list -> id        }} </td>
                        <td> {{$list -> name      }} </td>
                        <td> {{$list -> npm       }} </td>
                        <td> {{$list -> classId   }} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

  </body>
</html>