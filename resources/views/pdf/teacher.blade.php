<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    
    <div class="card-header">
            Teacher Table
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th> id</th>
                    <th> name</th>
                    <th> reg_id  </th>
                    <th> isActive   </th>
                    <th> classCount  </th>
                </tr>
            </thead>
            <tbody>
                @foreach($lists as $list)
                <tr>
                    <td> {{$list -> id        }} </td>
                    <td> {{$list -> name      }} </td>
                    <td> {{$list -> reg_id    }} </td>
                    <td> {{$list -> isActive  }} </td>
                    <td> {{$list -> classCount}} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

  </body>
</html>