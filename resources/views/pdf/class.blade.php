<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <!--section list.class-->
    <div class="card-header">
        Class Table - PDF Download -
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th> id</th>
                    <th> name</th>
                    <th> time </th>
                    <th> room </th>
                    <th> teacher_id</th>
                    <th> isOpen </th>
                    <th> semester</th>
                </tr>
            </thead>
            <tbody>
                @foreach($lists as $list)
                <tr>
                    <td> {{$list -> id        }} </td>
                    <td> {{$list -> name      }} </td>
                    <td> {{$list -> time      }} </td>
                    <td> {{$list -> room      }} </td>
                    <td> {{$list -> teacher_id}} </td>
                    <td> {{$list -> isOpen    }} </td>
                    <td> {{$list -> semester  }} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>