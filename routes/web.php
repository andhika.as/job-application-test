<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/routes', 'HomeController@admin')->middleware('admin');
Route::get('admin/addClass', 'AdminController@addClass')->middleware('admin');
Route::get('admin/addTeacher', 'AdminController@addTeacher')->middleware('admin');
Route::get('admin/addStudent', 'AdminController@addStudent')->middleware('admin');

Route::post('admin/addClass', [
    'as' => 'class.create',
    'uses' => 'AdminApp\ClassController@create'
])->middleware('admin');

Route::post('admin/addTeacher', [
    'as' => 'teacher.create',
    'uses' => 'AdminApp\TeacherController@create'
])->middleware('admin');

Route::post('admin/addStudent', [
    'as' => 'student.create',
    'uses'  => 'AdminApp\StudentController@create',
])->middleware('admin');
//downloadPDF
Route::get('admin/downloadPDF/{page}', 'AdminController@downloadPDF')->name('downloadPDF')->middleware('admin');

//EDIT
Route::get('admin/editClass/{id}', 'AdminController@editClass')->name('class.edit')->middleware('admin');
Route::get('admin/editTeacher/{id}', 'AdminController@editTeacher')->name('teacher.edit')->middleware('admin');
Route::get('admin/editStudent/{id}', 'AdminController@editStudent')->name('student.edit')->middleware('admin');

//MODIFY
Route::patch('admin/editClass/{id}', [
    'as' => 'class.modify',
    'uses' => 'AdminApp\ClassController@modify'
])->middleware('admin');

Route::patch('admin/editTeacher/{id}', [
    'as' => 'teacher.modify',
    'uses' => 'AdminApp\TeacherController@modify'
])->middleware('admin');

Route::patch('admin/editStudent/{id}', [
    'as' => 'student.modify',
    'uses'  => 'AdminApp\StudentController@modify',
])->middleware('admin');

// DELETE
Route::delete('admin/addClass/delete/{id}', [
    'as' => 'class.delete',
    'uses' => 'AdminApp\ClassController@delete'
])->middleware('admin');

Route::delete('admin/addTeacher/delete/{id}', [
    'as' => 'teacher.delete',
    'uses' => 'AdminApp\TeacherController@delete'
])->middleware('admin');

Route::delete('admin/addStudent/delete/{id}', [
    'as' => 'student.delete',
    'uses'  => 'AdminApp\StudentController@delete',
])->middleware('admin');