by Andhika for practice PHP

$ ./run.sh
OR Run the "run.sh" file 

Start:
0. Install depedencies (php, composer and stuff)
1. composer install
3. $ cp .env.example .env
4. configure mysql (as requied in .env.example)
    In Mysql:
        CREATE DATABASE homestead;
        USE homestead;
        GRANT ALL PRIVILEGES ON homestead.* TO 'homestead'@'localhost' IDENTIFIED BY 'secret';
5. php artisan make:auth
6. php artisan migrate
7. php artisan serve

Next: real coding

